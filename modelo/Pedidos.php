<?php
class Pedidos extends db{
	
    /* PEDIDOS */
    	public function getPedidos(){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos order by id DESC";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function setTotalPedido($idPedido, $total){
		$conn = $this->connect();
		$sql = "UPDATE pedidos SET total = $total where id = $idPedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		
		$conn->close();
		return $result;
	}
	public function calculaPrecioBases($codigoplanta,$tarifa, $bases){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				if ($tarifa == 1){
        			$precio = $row['precio_base'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
        		$plantasxbase = $row['plantas_base'];
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $bases * $plantasxbase;
		$conn->close();
		return $total;
	}
	public function calculaPrecioUnidades($codigoplanta,$tarifa, $unidades){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		if ($tarifa == 1){
        			$precio = $row['precio_unidad'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $unidades;
		$conn->close();
		return $total;
	}
	public function calculaPrecioCarros($codigoplanta,$tarifa, $carros){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		if ($tarifa == 1){
        			$precio = $row['precio_carro'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
        		$plantasxcarro = $row['plantas_carro'];
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $carros * $plantasxcarro;
		$conn->close();
		return $total;
	}
	public function setNuevoPedido($iduser, $total){
		$conn = $this->connect();
		$sql = "INSERT into pedidos (id,iduser,total)
			values ('',$iduser,$total)";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}
		public function getLineasPedido($idpedido){
		$conn = $this->connect();
		$sql = "SELECT * from lineaspedido where idpedido = $idpedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
		public function setNuevaLineaPedido($idPedido,$lineapedido,$codigoplanta,$pedidounidades,$pedidobases,$pedidocarros,$totalcarros,$totalbases,$totalunidades,$totallinea){
		$conn = $this->connect();
		$sql = "INSERT into lineaspedido (idpedido,idlinea,codigo,pedidocarros,pedidobases,pedidounidades,totalcarros,totalbases,totalunidades,totallinea)
			values ('$idPedido','$lineapedido','$codigoplanta', '$pedidounidades', '$pedidobases', '$pedidocarros', '$totalcarros', '$totalbases', '$totalunidades', '$totallinea')";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $lineapedido;
	}
}
?>