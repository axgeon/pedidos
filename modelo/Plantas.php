<?php
class Plantas extends db{
	
	/* PLANTAS */
	public function getPlantas(){
		$conn = $this->connect();
		$sql = "SELECT * from planta";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getPlantasDisponibles(){
		$conn = $this->connect();
		$sql = "SELECT * from planta where visible = 1";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function countPlantasDisponibles(){
		$conn = $this->connect();
		$sql = "SELECT COUNT(codigo) as total from planta where visible = 1";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		$total = $row["total"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
		public function getPlantasDisponiblesTipo($tipo){
		$conn = $this->connect();
		$sql = "SELECT * from planta where visible = 1 AND tipoplantaID = $tipo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	/* PLANTAS */

}
?>