<?php

class db{
	public function connect(){
		$conn = new mysqli("localhost", "root", "", "pedidos");
		// Check connection
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}else{
			//echo "conectado";
		}
		return $conn;

	}
	public  function checkLogin() {
			$user = $_POST["user"];
			$pass = $_POST["password"];
			$conn = $this->connect();
			$login=FALSE;

			$sql = "SELECT user,password FROM usuarios WHERE user = '$user' AND password = '$pass'";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
    			$user=$row["user"];
    			$pass=$row["password"];
    			$login=true;
    			$_SESSION["login"] = $login;
    			$_SESSION["user"] = $user;
    			
			}
			}else {
			//echo "Error: " . $sql . "<br>" . $conn->error;
			$login =false;
			}

			$conn->close();
			return $login;
	}
	public  function cerrarSesion() {
        session_destroy();
    }


    /*COMPRAS*/
    public function setNuevaCompra($idproveedor,$idcliente,$total){
		$conn = $this->connect();
		$sql = "INSERT into compras (id,idproveedor,idcliente,total,fecha)
			values ('',$idproveedor,$idcliente,$total,now())";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}
	public function setNuevaLineaCompra($idPedido,$lineapedido,$codigoplanta,$pedidounidades,$pedidobases,$pedidocarros){
		$conn = $this->connect();
		$sql = "INSERT into lineascompras (idpedido,idlinea,codigo,pedidocarros,pedidobases,pedidounidades)
			values ('$idPedido','$lineapedido','$codigoplanta', '$pedidounidades', '$pedidobases', '$pedidocarros')";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $lineapedido;
	}
    public function GetCompras(){
		$conn = $this->connect();
		$sql = "SELECT * from compras order by id DESC";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getLineasCompra($idcompra){
		$conn = $this->connect();
		$sql = "SELECT * from lineascompras where idpedido = $idcompra ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
    /*COMPRAS*/
    /* PEDIDOS */
    	public function getPedidos(){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos order by id DESC";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
    public function getPedidosConfirmados(){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos where estado = '1'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
    public function getPedidosComprados(){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos where estado = '2'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}	
	public function countPedidos(){
	$conn = $this->connect();
	$sql = "SELECT count(*) as total from pedidos";
	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$total = $row['total'];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
	return $total;
	}
	public function countComprasPendientes(){
	$conn = $this->connect();
	$sql = "SELECT count(*) as total from pedidos where estado = 1";
	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$total = $row['total'];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
	return $total;
	}
    public function getPedidosByCliente($codigo){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos where iduser = $codigo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
    public function getPedidoById($id){
		$conn = $this->connect();
		$sql = "SELECT * from pedidos where id = $id";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$pedido = $result->fetch_assoc();
		} else {
   			//echo "0 results";
		}


		$conn->close();
		return $pedido;
	}
	
	public function setTotalPedido($idPedido, $total){
		$conn = $this->connect();
		$sql = "UPDATE pedidos SET total = $total where id = $idPedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		
		$conn->close();
		return $result;
	}
	public function setPedidoConfirmado($idPedido){
		$conn = $this->connect();
		$sql = "UPDATE pedidos SET estado = '1' where id = '$idPedido' ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $result;
	}
	public function setPedidoComprado($idPedido){
		$conn = $this->connect();
		$sql = "UPDATE pedidos SET estado = '2' where id = '$idPedido' ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $result;
	}	
	public function calculaPrecioBases($codigoplanta,$tarifa, $bases){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				if ($tarifa == 1){
        			$precio = $row['precio_base'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
        		$plantasxbase = $row['plantas_base'];
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $bases * $plantasxbase;
		$conn->close();
		return $total;
	}
	public function calculaPrecioUnidades($codigoplanta,$tarifa, $unidades){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		if ($tarifa == 1){
        			$precio = $row['precio_unidad'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $unidades;
		$conn->close();
		return $total;
	}
	public function calculaPrecioCarros($codigoplanta,$tarifa, $carros){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigoplanta'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		if ($tarifa == 1){
        			$precio = $row['precio_carro'];

        		}else{
        			$precio = $row['precio_especial'];

        		}
        		$plantasxcarro = $row['plantas_carro'];
   			}
		} else {
   			//echo "0 results";
		}
		$total = $precio * $carros * $plantasxcarro;
		$conn->close();
		return $total;
	}
		public function getClientePedido($idpedido){
		$conn = $this->connect();
		$sql = "SELECT iduser from pedidos where idpedido = $idpedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        			$cliente = $row['iduser'];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $cliente;
	}
	public function setNuevoPedido($iduser, $total,$tarifa){
		$conn = $this->connect();
		$sql = "INSERT into pedidos (id,iduser,total,fecha,tarifa)
			values ('',$iduser,$total,now(),$tarifa)";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}
		public function getLineasPedido($idpedido){
		$conn = $this->connect();
		$sql = "SELECT * from lineaspedido where idpedido = $idpedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function borrarLineasPedido($idpedido){
		$conn = $this->connect();
		$sql = "DELETE from lineaspedido where idpedido = $idpedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		/*if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}
*/
		$conn->close();
		return $result;
	}
	public function getLineasPedidoConNombre($idpedido){
		$conn = $this->connect();
		$sql = "SELECT * FROM lineaspedido LEFT JOIN planta ON lineaspedido.codigo = planta.codigo WHERE lineaspedido.idpedido = $idpedido ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getLineasPedidosConfirmados(){
		$conn = $this->connect();
		$sql = "SELECT * FROM lineaspedido INNER JOIN pedidos on lineaspedido.idpedido = pedidos.id where pedidos.estado = 1 ORDER BY codigo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
		public function setNuevaLineaPedido($idPedido,$lineapedido,$codigoplanta,$pedidounidades,$pedidobases,$pedidocarros,$totalcarros,$totalbases,$totalunidades,$totallinea){
		$conn = $this->connect();
		$sql = "INSERT into lineaspedido (idpedido,idlinea,codigo,pedidocarros,pedidobases,pedidounidades,totalcarros,totalbases,totalunidades,totallinea)
			values ('$idPedido','$lineapedido','$codigoplanta', '$pedidounidades', '$pedidobases', '$pedidocarros', '$totalcarros', '$totalbases', '$totalunidades', '$totallinea')";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $lineapedido;
	}
	/* PROVEEDORES */
	public function getProveedores(){
		$conn = $this->connect();
		$sql = "SELECT * from proveedores";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getProveedor($codigo){
		$conn = $this->connect();
		$sql = "SELECT * from proveedores WHERE codigo = $codigo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$proveedor = $result->fetch_assoc();
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $proveedor;



	}	
	public function setProveedor($nombre,$nombre2,$direccion,$codpost,$poblacion,$provincia,$cif,$email){
		$conn = $this->connect();
		$sql = "INSERT INTO proveedores (codigo,nombre,nombre2,direccion,codpost,poblacion,provincia,cif,email,registro)
			values ('','$nombre','$nombre2','$direccion','$codpost','$poblacion','$provincia','$cif','$email',now())";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;



	}
	public function updateProveedor($codigo,$nombre,$nombre2,$direccion,$codpost,$poblacion,$provincia,$cif,$email){
		$conn = $this->connect();
		$sql = "UPDATE proveedores SET nombre = '$nombre',nombre2 = '$nombre2',direccion = '$direccion',codpost = '$codpost',poblacion = '$poblacion',provincia = '$provincia',cif = '$cif' ,email = '$email'
			WHERE codigo = $codigo";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;



	}
	/* PROVEEDORES */
	/* CLIENTES */
	public function getClientes(){
		$conn = $this->connect();
		$sql = "SELECT * from clientes";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getCliente($codigo){
		$conn = $this->connect();
		$sql = "SELECT * from clientes WHERE codigo = $codigo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$cliente = $result->fetch_assoc();
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $cliente;



	}
	public function countClientes(){
	$conn = $this->connect();
	$sql = "SELECT count(*) as total from clientes";
	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$total = $row['total'];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
	return $total;
	}	
		public function setCliente($nombre,$nombre2,$direccion,$codpost,$poblacion,$provincia,$cif,$email){
		$conn = $this->connect();
		$sql = "INSERT INTO clientes (codigo,nombre,nombre2,direccion,codpost,poblacion,provincia,cif,email,registro)
			values ('','$nombre','$nombre2','$direccion','$codpost','$poblacion','$provincia','$cif','$email',now())";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;



	}
	public function updateCliente($codigo,$nombre,$nombre2,$direccion,$codpost,$poblacion,$provincia,$cif,$email){
		$conn = $this->connect();
		$sql = "UPDATE clientes SET nombre = '$nombre',nombre2 = '$nombre2',direccion = '$direccion',codpost = '$codpost',poblacion = '$poblacion',provincia = '$provincia',cif = '$cif' ,email = '$email'
			WHERE codigo = $codigo";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;



	}
	/* CLIENTES */
	/* PLANTAS */
	public function getPlantas(){
		$conn = $this->connect();
		$sql = "SELECT * from planta";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getPlantasDisponibles(){
		$conn = $this->connect();
		$sql = "SELECT * from planta where visible = 1";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getPlantasByProveedor($codigo){
		$conn = $this->connect();
		$sql = "SELECT * from planta_proveedor where codigoproveedor = $codigo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getPlantas_Proveedor(){
		$conn = $this->connect();
		$sql = "SELECT * from planta_proveedor LEFT JOIN planta ON planta_proveedor.codigoplanta = planta.codigo ";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}	
    public function getPlantaByCodigo($codigo){
		$conn = $this->connect();
		$sql = "SELECT * from planta where codigo = '$codigo'";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$planta = $result->fetch_assoc();
		} else {
   			//echo "0 results";
		}


		$conn->close();
		return $planta;

	}
	public function countPlantasDisponibles(){
		$conn = $this->connect();
		$sql = "SELECT COUNT(codigo) as total from planta where visible = 1";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		$total = $row["total"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $total;
	}
		public function getPlantasDisponiblesTipo($tipo){
		$conn = $this->connect();
		$sql = "SELECT * from planta where visible = 1 AND tipoplantaID = $tipo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	/* PLANTAS */
	/* OTROS*/
	public function setPosibleReserva($oficinarecogida,$fecharecogida,$horarecogida,$minutosrecogida,$oficinadevolucion,$fechadevolucion,$horadevolucion,$minutosdevolucion){
		$conn = $this->connect();
		$sql = "INSERT into posiblesreservas (id,oficinarecogida,fecharecogida,horarecogida,minutosrecogida,
			oficinadevolucion,fechadevolucion,horadevolucion,minutosdevolucion)
			values ('', '$oficinarecogida', '$fecharecogida', '$horarecogida',
			'$minutosrecogida', '$oficinadevolucion', '$fechadevolucion', '$horadevolucion' ,'$minutosdevolucion')";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}
	public function getDatosReserva($reserva){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas where id = ".$reserva."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
    			$reserva=[
					"horarecogida" => $row["horarecogida"],
					"horadevolucion" => $row["horadevolucion"],
					"fecharecogida" => $row["fecharecogida"],
					"fechadevolucion" => $row["fechadevolucion"],
					"minutosrecogida" => $row["minutosrecogida"],
					"minutosdevolucion" => $row["minutosdevolucion"],
				];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $reserva;
	}
		public function getModeloReserva($reserva){
		$conn = $this->connect();
		$sql = "SELECT modelo from posiblesreservas where id = ".$reserva."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $row["modelo"];
	}
		public function setNuevoVehiculo($modelo,$grupo,$plazas,$puertas,$aa,$mp3,$volumenmaletero,$consumomedio,$combustible){
		$conn = $this->connect();
		$sql = "INSERT into vehiculos (id, modelo, grupo, plazas, puertas, aa, mp3, volumenmaletero, consumomedio, combustivle )
			values ('', '$modelo', '$grupo', '$plazas',
			'$puertas', '$aa', '$mp3', '$volumenmaletero' ,'$consumomedio','$combustible')";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}

		public function getPosiblesReservas(){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
		public function getReservas(){
		$conn = $this->connect();
		$sql = "SELECT * from reserva";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
		public function getExtras(){
		$conn = $this->connect();
		$sql = "SELECT * from extras";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $result;
	}
	public function getVehiculo($id){
		$conn = $this->connect();
		$sql = "SELECT * from vehiculos where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo $row["id"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $row;
	}

	public function getNombreCliente($id){
		$conn = $this->connect();
		$sql = "SELECT * from clientes where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$nombrecliente = $row["nombre"];
        		$apellidoscliente = $row["apellidos"];
        		$nombrecompleto = $nombrecliente." ".$apellidoscliente;
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $nombrecompleto;
	}
		public function getOficinaRecogida($id){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$oficinarecogida = $row["oficinarecogida"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $oficinarecogida;
	}
		public function getOficinaDevolucion($id){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$oficinadevolucion = $row["oficinadevolucion"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $oficinadevolucion;
	}
		public function getDesde($id){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$fecharecogida = $row["fecharecogida"];
        		$horarecogida = $row["horarecogida"];
        		$minutosrecogida = $row["minutosrecogida"];
        			$fechaconyear = $fecharecogida; 
					$parte = explode("-", $fechaconyear);
					//var_dump($parte);
					$fechaconyear = $parte[2]."-".$parte[1]."-".$parte[0];
        		$desde = $fechaconyear." a las ".$horarecogida.":".$minutosrecogida;
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $desde;
	}
		public function getHasta($id){
		$conn = $this->connect();
		$sql = "SELECT * from posiblesreservas where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$fechadevolucion = $row["fechadevolucion"];
        		$horadevolucion = $row["horadevolucion"];
        		$minutosdevolucion = $row["minutosdevolucion"];
        		$fechaconyear = $fechadevolucion; 
					$parte = explode("-", $fechaconyear);
					//var_dump($parte);
					$fechaconyear = $parte[2]."-".$parte[1]."-".$parte[0];
        		$hasta = $fechaconyear." a las ".$horadevolucion.":".$minutosdevolucion;
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $hasta;
	}
		public function getNombreOficina($idoficina){
		$conn = $this->connect();
		$sql = "SELECT nombre from oficinas where id = ".$idoficina."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$nombre = $row["nombre"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $nombre;
	}
		public function getModeloVehiculo($id){
		$conn = $this->connect();
		$sql = "SELECT * from vehiculos where id = ".$id."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$nombremodelo = $row["modelo"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $nombremodelo;
	}
	public function getTarifaActual(){//para saber si es temporada alta o baja, y luego coger los precios adecuados
		$conn = $this->connect();
		$sql = "SELECT * from tarifa";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$tarifaactual = $row["tarifaactual"];
        		//echo $tarifaactual;
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $tarifaactual;
	}
	public function getPrecioTotalExtras($idextras){

		$conn = $this->connect();
		$sql = "SELECT * from reserva where idextras = ".$idextras."";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		$precioextras = $row["precioextras"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $precioextras;
	}

	public function setReserva($reserva,$modeloreserva,$idcliente){
		$conn = $this->connect();
		$sql = "INSERT into reserva (id,id_posiblereserva,id_vehiculo,id_cliente)
			values ('',$reserva,$modeloreserva,$idcliente)";
		if (mysqli_query($conn, $sql)) {
			//echo "New record created successfully";
			$last_id = $conn->insert_id;
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();
		return $last_id;
	}
	
		public function getPrecioDiasAlta($dias,$modelo){
		$conn = $this->connect();
		$sql = "SELECT * from tarifa_alta where id_vehiculo = $modelo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		$precio =  $row["$dias"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $precio;
	}
		public function getPrecioDiasBaja($dias,$modelo){
		$conn = $this->connect();
		$sql = "SELECT * from tarifa_baja where id_vehiculo = $modelo";
		//echo $sql."<br/>";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		//echo "id: " . $row["id"]. "<br/>";
        		$precio =  $row["$dias"];
   			}
		} else {
   			//echo "0 results";
		}

		$conn->close();
		return $precio;
	}

}
?>