<?php
require_once("app.php");
	if(isset($_POST['action']) && $_POST['action'] == 'verpedido'){
				//echo 'estamos en verpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedidoConNombre($_POST['idpedido']);
		
		$cliente = $db->getCliente($pedido['iduser']);
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'modificarpedido'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedidoConNombre($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$plantastipo1 = $db->getPlantasDisponiblesTipo(1);
		$plantastipo2 = $db->getPlantasDisponiblesTipo(2);
		$plantastipo3 = $db->getPlantasDisponiblesTipo(3);
		$plantastipo4 = $db->getPlantasDisponiblesTipo(4);
		$plantastipo5 = $db->getPlantasDisponiblesTipo(5);
		$plantastipo6 = $db->getPlantasDisponiblesTipo(6);
		$tarifa = $pedido['tarifa'];
		$plantas = $db->getPlantasDisponibles();
		//$idPedido = $db->setNuevoPedido($cliente['codigo'],$pedido['total'],$pedido['tarifa']);
		//foreach ($lineaspedido as $lineapedido){
		//$lineapedido = $db->setNuevaLineaPedido($idPedido,$lineapedido['idlinea'],$lineapedido['codigo'],$lineapedido['pedidocarros'],$lineapedido['pedidobases'],$lineapedido['pedidounidades'],$lineapedido['totalcarros'],$lineapedido['totalbases'],$lineapedido['totalunidades'],$lineapedido['totallinea']);	
		//}
		//$pedido = $db->getPedidoById($idPedido);
		
		require_once("../pages/index.php");
		require_once("../pages/modificarpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'duplicarpedido'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedidoConNombre($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$idPedido = $db->setNuevoPedido($cliente['codigo'],$pedido['total'],$pedido['tarifa']);
		foreach ($lineaspedido as $lineapedido){
		$lineapedido = $db->setNuevaLineaPedido($idPedido,$lineapedido['idlinea'],$lineapedido['codigo'],$lineapedido['pedidocarros'],$lineapedido['pedidobases'],$lineapedido['pedidounidades'],$lineapedido['totalcarros'],$lineapedido['totalbases'],$lineapedido['totalunidades'],$lineapedido['totallinea']);	
		}
		$pedido = $db->getPedidoById($idPedido);
		$a->ok("Pedido duplicado con éxito");
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'confirmarpedido'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedidoConNombre($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$resultado = $db->setPedidoConfirmado($_POST['idpedido']);
		$a->ok("Pedido confirmado con éxito");
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'pedidocomprado'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedidoConNombre($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$resultado = $db->setPedidoComprado($_POST['idpedido']);
		$a->ok("Pedido comprado con éxito");
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else{
		$pedidos = $db->getPedidos();
		require_once("../pages/index.php");
		require_once("../pages/verpedidos.php");
		require_once("../pages/footer.php");
	}

?>