<?php
require_once("app.php");
	if(isset($_POST['action']) && $_POST['action'] == 'verpedido'){

		$lineaspedido = $db->getLineasPedido($_POST['idpedido']);
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'confirmarpedido'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedido($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$resultado = $db->setComprado($_POST['idpedido']);
		$a->ok("Compra confirmada con éxito");
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else{
		$pedidos = $db->getPedidosConfirmados();

		require_once("../pages/index.php");

		require_once("../pages/vercompraspendientes.php");
		foreach ($pedidos as $pedido) {
			$lineaspedido = $db->getLineasPedido($pedido['id']);
			require("../pages/verpedido_partial.php");
		}
		require_once("../pages/footer.php");
	}

?>