<?php
require_once("app.php");
	if(isset($_POST['action']) && $_POST['action'] == 'verpedido'){

		$lineaspedido = $db->getLineasCompra($_POST['idpedido']);
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else if(isset($_POST['action']) && $_POST['action'] == 'confirmarpedido'){
		//echo 'estamos en confirmarpedido';
		$pedido = $db->getPedidoById($_POST['idpedido']);
		$lineaspedido = $db->getLineasPedido($_POST['idpedido']);
		$cliente = $db->getCliente($pedido['iduser']);
		$resultado = $db->setComprado($_POST['idpedido']);
		
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
	}else{
		$pedidos = $db->getCompras();

		require_once("../pages/index.php");

		require_once("../pages/vercomprasrealizadas.php");
		foreach ($pedidos as $pedido) {
			$lineaspedido = $db->getLineasCompra($pedido['id']);
			require("../pages/vercompras_partial.php");
		}
		require_once("../pages/footer.php");
	}

?>