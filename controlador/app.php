<?php
//Para mostrar errores de php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);
//Cargamos el modelo
require_once("../modelo/db.php");
$db=new db();
session_start();
require_once("alertas.php");
$a=new alertas();
//comprobamos sesion
if (!isset($_SESSION["user"])) {
	$login = $db->checkLogin();
	
}
if(!isset($_SESSION["login"]) || $_SESSION["login"]==FALSE){
		require_once("../pages/login.php");
	}
if(isset($_POST['action']) && $_POST['action'] == 'cerrar'){
		$db->cerrarSesion();
		header('Location: ../pages/login.php');
	}

?>