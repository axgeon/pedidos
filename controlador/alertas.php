<?php
class alertas{
	public function ok($alerta){
		echo '<div class="alert alert-success alert-dismissable">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>'.$alerta.'</strong>
		</div>';
	}
	public function error($error){
		echo '<div class="alert alert-danger alert-dismissable">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>'.$error.'</strong>
		</div>';
	}
}
?>