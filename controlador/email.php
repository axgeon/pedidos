<?php
class Email{
	public function enviarMailCliente($datospersonalesarray){
		$to = $datospersonalesarray['email'];
 		$subject = "Su pre-reserva en AlboranAutos ha sido enviada";
 		$nombre = $datospersonalesarray['nombre'];
 		$body = "Hola $nombre ,\n\nTu pre-reserva ha sido realizada correctamente, nuestro personal se pondra en contacto contigo lo antes posible para confirmar la reserva";
 		if (mail($to, $subject, $body)) {
   			//echo("<p>Email enviado!</p>");
  		} else {
   			//echo("<p>Email fail</p>");
  		}	
 	}
 	public function enviarMailAlboran($datospersonalesarray,$datosreserva,$nombremodelo){
 		$to = 'programacion1@divabercom.net';
 		$subject = "Se ha realizado una pre-reserva en AlboranAutos";
 		$nombre = $datospersonalesarray['nombre'];
 		$diainicio = $datosreserva['fecharecogida'];
 		$diafin = $datosreserva['fechadevolucion'];
 		$horainicio = $datosreserva['horarecogida'];
 		$horafin = $datosreserva['horadevolucion'];
    $minutosinicio = $datosreserva['minutosrecogida'];
    $minutosdevolucion = $datosreserva['minutosdevolucion'];
 		$email = $datospersonalesarray['email'];
    $dias = ((strtotime($diafin)-strtotime($diainicio))/86400);
 		$body = "$nombre Ha realizado una pre-reserva correctamente,solicita el vehiculo ".$nombremodelo." durante 
    ".$dias." dias desde el dia ".$diainicio." a la hora ".$horainicio.":".$minutosinicio." al dia ".$diafin." hora ".$horafin.":".$minutosinicio." Puede ponerse en contacto con el cliente para confirmar la reserva en el correo ".$email;
 		if (mail($to, $subject, $body)) {
   			//echo("<p>Email enviado!</p>");
  		} else {
   			//echo("<p>Email fail</p>");
  		}

 	}
}
?>