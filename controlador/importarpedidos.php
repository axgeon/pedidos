<?php
require_once("app.php");

	 if (isset($_POST['action']) && $_POST['action'] == 'uploadtipo1'){
	 	$numeroplantas = $db->countPlantasDisponibles();
	 	$tarifa = 1;
		//cargamos el archivo al servidor con el mismo nombre
		//solo le agregue el sufijo bak_
		$archivo = $_FILES['excel']['name'];
		$tipo = $_FILES['excel']['type'];
		$destino = "bak_".$archivo;
		if (copy($_FILES['excel']['tmp_name'],$destino)){
			//echo "Archivo Cargado Con Éxito";	
			$a->ok("archivo cargado con éxito");
		} 
		else{
			//echo "Error Al Cargar el Archivo";
			$a->error("error al cargar el archivo");	
		} 
		if (file_exists ("bak_".$archivo)){
			/** Clases necesarias */
			require_once('../Classes/PHPExcel.php');
			require_once('../Classes/PHPExcel/Reader/Excel2007.php');
			// Cargando la hoja de cálculo
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$objPHPExcel = $objReader->load("bak_".$archivo);
			$objFecha = new PHPExcel_Shared_Date();
			// Asignar hoja de excel activa
			$objPHPExcel->setActiveSheetIndex(0);
			// Pasamos los datos del excel a un array para trabajar con ellos
			for ($i=1;$i<=$numeroplantas;$i++){
				$_DATOS_EXCEL[$i]['pedidocarros'] = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
				$_DATOS_EXCEL[$i]['pedidobases'] = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
				$_DATOS_EXCEL[$i]['pedidounidades']= $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
				$_DATOS_EXCEL[$i]['codigoplanta']= $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
			}
		}
		//si por algo no cargo el archivo bak_
		else{echo "Necesitas primero importar el archivo";}
		$errores=0;
		//recorremos el array
		//para ir recuperando los datos obtenidos
		//del excel e ir insertandolos en la BD
		$idPedido = $db->setNuevoPedido($_POST['codigo'],0,$tarifa);
		$lineapedido = 0;
		$totalAbsoluto = 0;
		foreach($_DATOS_EXCEL as $campo){

			if($campo['pedidounidades'] > 0 || $campo['pedidobases'] > 0 || $campo['pedidocarros'] >0){
					$lineapedido = $lineapedido + 1;
					$totalunidades = $db->calculaPrecioUnidades($campo['codigoplanta'],$tarifa,$campo['pedidounidades']);
					$totalbases = $db->calculaPrecioBases($campo['codigoplanta'],$tarifa,$campo['pedidobases']);
					$totalcarros = $db->calculaPrecioCarros($campo['codigoplanta'],$tarifa,$campo['pedidocarros']);
					$totallinea = $totalunidades + $totalbases + $totalcarros;
					$totalAbsoluto = $totalAbsoluto + $totallinea;
							$lineapedido = $db->setNuevaLineaPedido($idPedido,$lineapedido,$campo['codigoplanta'],$campo['pedidocarros'],$campo['pedidobases'],$campo['pedidounidades'],$totalcarros,$totalbases,$totalunidades,$totallinea);
						//echo $lineapedido;
			}
		}
		$db->setTotalPedido($idPedido, $totalAbsoluto);
		$lineaspedido = $db->getLineasPedido($idPedido);
		$cliente = $db->getCliente($_POST['codigo']);
			$pedido = $db->getPedidoById($idPedido);
		$alerta = "Pedido #".$idPedido." importado con exito";
		require_once("../pages/index.php");
		require_once("../pages/verpedido.php");
		require_once("../pages/footer.php");
		//echo "<strong><center>ARCHIVO IMPORTADO CON EXITO, EN TOTAL $campo REGISTROS Y $errores ERRORES</center></strong>";
		//una vez terminado el proceso borramos el
		//archivo que esta en el servidor el bak_
		unlink($destino);
		}else if(isset($_POST['action']) && $_POST['action'] == 'uploadtipo2'){
			$numeroplantas = $db->countPlantasDisponibles();
			$tarifa = 2;
						//cargamos el archivo al servidor con el mismo nombre
			//solo le agregue el sufijo bak_
			$archivo = $_FILES['excel']['name'];
			$tipo = $_FILES['excel']['type'];
			$destino = "bak_".$archivo;
			if (copy($_FILES['excel']['tmp_name'],$destino)){
				//echo "Archivo Cargado Con Éxito";	
				$a->ok("archivo cargado con éxito");

			} 
			else{
				$a->error("error al cargar el archivo");
			} 
			if (file_exists ("bak_".$archivo)){
				/** Clases necesarias */
				require_once('../Classes/PHPExcel.php');
				require_once('../Classes/PHPExcel/Reader/Excel2007.php');
				// Cargando la hoja de cálculo
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
				$objPHPExcel = $objReader->load("bak_".$archivo);
				$objFecha = new PHPExcel_Shared_Date();
				// Asignar hoja de excel activa
				$objPHPExcel->setActiveSheetIndex(0);
				// Pasamos los datos del excel a un array para trabajar con ellos
				for ($i=1;$i<=$numeroplantas;$i++){
					$_DATOS_EXCEL[$i]['pedidocarros'] = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
					$_DATOS_EXCEL[$i]['pedidobases'] = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
					$_DATOS_EXCEL[$i]['pedidounidades']= $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
					$_DATOS_EXCEL[$i]['codigoplanta']= $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
				}
			}
			//si por algo no cargo el archivo bak_
			else{echo "Necesitas primero importar el archivo";}
			$errores=0;
			//recorremos el array
			//para ir recuperando los datos obtenidos
			//del excel e ir insertandolos en la BD
			$idPedido = $db->setNuevoPedido($_POST['codigo'],0,$tarifa);
			$lineapedido = 0;
			$totalAbsoluto = 0;
			foreach($_DATOS_EXCEL as $campo){

				if($campo['pedidounidades'] > 0 || $campo['pedidobases'] > 0 || $campo['pedidocarros'] >0){
						$lineapedido = $lineapedido + 1;
						$totalunidades = $db->calculaPrecioUnidades($campo['codigoplanta'],$tarifa,$campo['pedidounidades']);
						$totalbases = $db->calculaPrecioBases($campo['codigoplanta'],$tarifa,$campo['pedidobases']);
						$totalcarros = $db->calculaPrecioCarros($campo['codigoplanta'],$tarifa,$campo['pedidocarros']);
						$totallinea = $totalunidades + $totalbases + $totalcarros;
						$totalAbsoluto = $totalAbsoluto + $totallinea;
								$lineapedido = $db->setNuevaLineaPedido($idPedido,$lineapedido,$campo['codigoplanta'],$campo['pedidocarros'],$campo['pedidobases'],$campo['pedidounidades'],$totalcarros,$totalbases,$totalunidades,$totallinea);
							//echo $lineapedido;
				}
			}
			$db->setTotalPedido($idPedido, $totalAbsoluto);
			$lineaspedido = $db->getLineasPedido($idPedido);
			$cliente = $db->getCliente($_POST['codigo']);
			$pedido = $db->getPedidoById($idPedido);
			$alerta = "Pedido #".$idPedido." importado con exito";
			require_once("../pages/index.php");
			require_once("../pages/verpedido.php");
			require_once("../pages/footer.php");
//			echo "<strong><center>ARCHIVO IMPORTADO CON EXITO, EN TOTAL $campo REGISTROS Y $errores ERRORES</center></strong>";
			//una vez terminado el proceso borramos el
			//archivo que esta en el servidor el bak_
			unlink($destino);

		}else{
			$clientes = $db->getClientes();
			require_once("../pages/index.php");
			require_once("../pages/importarpedidos.php");
			require_once("../pages/footer.php");
	}


?>