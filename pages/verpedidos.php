
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pedidos</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de pedidos</div>
                
                <div class="panel-body">
<div>
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline pedidos" id="pedidos" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">#</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">Cliente</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">Total</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">Tarifa</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">Fecha</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">Estado</th>
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($pedidos as $pedido) {?>
    <tr>
      <td><?php echo $pedido['id'] ?></td>
      <td><?php echo $pedido['iduser'] ?></td>
      <td><?php echo $pedido['total'] ?>€</td>
      <td><?php echo $pedido['tarifa'] ?></td>
      <td><?php echo $pedido['fecha'] ?></td>
      <td><?php if($pedido['estado'] == 1){
        echo '<i class="fa fa-file" style="color:green;"> Confirmado';
      }else if($pedido['estado'] == 0){
        echo '<i class="fa fa-close" style="color:red;"> Borrador';
      }else if($pedido['estado'] == 2){
        echo '<i class="fa fa-shopping-cart" style="color:blue;"> Comprado';
      }

      ?></td>

      <td>
        <form action="../controlador/pedidos.php" method="POST" style="float:left;">
          <input type="hidden" name="action" value="modificarpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $pedido['id'] ?>" />
          <input type="submit" value="Modificar" />
        </form>
        <form action="../controlador/pedidos.php" method="POST" style="float:left;">
          <input type="hidden" name="action" value="duplicarpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $pedido['id'] ?>" />
          <input type="submit" value="Duplicar" />
        </form>
        <form action="../controlador/pedidos.php" method="POST" >
          <input type="hidden" name="action" value="verpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $pedido['id'] ?>" />
          <input type="submit" value="Ver" />
        </form>
      
      </td>
      
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

