        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Plantas</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de pedidos</div>
                
                <div class="panel-body">
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline nowrap" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">plantaid</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Browser: activate to sort column ascending">codigo</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">descripcion</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">tipoplantaID</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">precio_coste</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">%_base</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">%_carro</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">%_unidad</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">precio_base</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">precio_unidad</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">precio_carro</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">porcentaje_especial</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">precio_especial</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">plantas_carro</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">plantas_base</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">ocup</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">visible</th>
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($plantas as $planta) {?>
    <tr>
      <td><?php echo $planta['plantaID'] ?></td>
      <td><?php echo $planta['codigo'] ?></td>
      <td><?php echo $planta['descripcion'] ?></td>
      <td><?php echo $planta['tipoplantaID'] ?></td>
      <td><?php echo $planta['precio_coste'] ?></td>
      <td><?php echo $planta['porcentaje_base'] ?></td>
      <td><?php echo $planta['porcentaje_carro'] ?></td>
      <td><?php echo $planta['porcentaje_unidad'] ?></td>
      <td><?php echo $planta['precio_base'] ?></td>
      <td><?php echo $planta['precio_unidad'] ?></td>
      <td><?php echo $planta['precio_carro'] ?></td>
      <td><?php echo $planta['porcentaje_especial'] ?></td>
      <td><?php echo $planta['precio_especial'] ?></td>
      <td><?php echo $planta['plantas_carro'] ?></td>
      <td><?php echo $planta['plantas_base'] ?></td>
      <td><?php echo $planta['ocup'] ?></td>
      <td><?php echo $planta['visible'] ?></td>
      <td>
        <form action="../controlador/pedidos.php" method="POST" style="float:left;">
          <input type="hidden" name="action" value="modificarpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $planta['id'] ?>" />
          <input type="submit" value="Modificar" />
        </form>

        <form action="../controlador/pedidos.php" method="POST" >
          <input type="hidden" name="action" value="verpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $planta['id'] ?>" />
          <input type="submit" value="Ver" />
        </form>
      
      </td>
      
    </tr>
    <?php
  }
  ?>
</tbody>
</table>
</div>

  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->