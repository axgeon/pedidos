
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pedidos</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Importar pedido</div>
                
                <div class="panel-body">
<div class="col-lg-6">
<h3>Tipo 1</h3>
                <form action="../controlador/importarpedidos.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="action" value="importexceltipo1" />
           <div class="form-group">
          <label for="sel1">Cliente:</label>
          <select class="form-control" id="sel1" name="codigo">
          <?php foreach($clientes as $cliente){
            echo "<option value='".$cliente['codigo']."'>".$cliente['codigo']." - ".$cliente['nombre']."</option>";
          }?>
          </select>
        </div> 

  <input type="file" name="excel" /><input type="hidden" value="uploadtipo1" name="action" />


    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Enviar</button>
      </div>
    </div>
  </form>
  </div>
  <div class="col-lg-6">
  <h3>Tipo 2</h3>
                  <form action="../controlador/importarpedidos.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="action" value="importexceltipo2" />
          <div class="form-group">
          <label for="sel1">Cliente:</label>
          <select class="form-control" id="sel1" name="codigo">
          <?php foreach($clientes as $cliente){
            echo "<option value='".$cliente['codigo']."'>".$cliente['codigo']." - ".$cliente['nombre']."</option>";
          }?>
          </select>
        </div> 

  <input type="file" name="excel" /><input type="hidden" value="uploadtipo2" name="action" />


    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Enviar</button>
      </div>
    </div>
  </form>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

