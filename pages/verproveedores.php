
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Proveedores</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de proveedores</div>
                
                <div class="panel-body">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">codigo</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">nombre</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">nombre2</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 255px;" aria-label="Engine version: activate to sort column ascending">direccion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">codpost</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">poblacion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">provincia</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">cif</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">email</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">registro</th>
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($proveedores as $proveedor) {?>
    <tr>
      <td><?php echo $proveedor['codigo'] ?></td>
      <td><?php echo $proveedor['nombre'] ?></td>
      <td><?php echo $proveedor['nombre2'] ?></td>
      <td><?php echo $proveedor['direccion'] ?></td>
      <td><?php echo $proveedor['codpost'] ?></td>
      <td><?php echo $proveedor['poblacion'] ?></td>
      <td><?php echo $proveedor['provincia'] ?></td>
      <td><?php echo $proveedor['cif'] ?></td>
      <td><?php echo $proveedor['email'] ?></td>
      <td><?php echo $proveedor['registro'] ?></td>
      <td>
        <form action="../controlador/proveedores.php" method="post">
        <input type="hidden" name="codigo" value="<?php echo $proveedor['codigo'] ?>" />
        <input type="hidden" name="action" value="modificar" />
          <input type="submit" value="Modificar" />
        </form>
      </td>
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

