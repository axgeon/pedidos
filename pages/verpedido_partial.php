
                <div class="panel panel-primary">
                <div class="panel-heading">Pedido #<?php echo $pedido['id'] ?></div>
                
                <div class="panel-body">
                <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">idpedido</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">idlinea</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">codigo</th>

      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">pedidocarros</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">pedidobases</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">pedidounidades</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">totalcarros</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">totalbases</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">totalunidades</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">totallinea</th>                        
     
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($lineaspedido as $lineapedido) {?>
    <tr>
      <td><?php echo $lineapedido['idpedido'] ?></td>
      <td><?php echo $lineapedido['idlinea'] ?></td>
      <td><?php echo $lineapedido['codigo'] ?></td>
      <td><?php echo $lineapedido['pedidocarros'] ?></td>
      <td><?php echo $lineapedido['pedidobases'] ?></td>
      <td><?php echo $lineapedido['pedidounidades'] ?></td>
            <td><?php echo $lineapedido['totalcarros'] ?></td>
      <td><?php echo $lineapedido['totalbases'] ?></td>
      <td><?php echo $lineapedido['totalunidades'] ?></td>
      <td><?php echo $lineapedido['totallinea'] ?></td>



      <td>
        <form action="../controlador/admin.php" method="post" style="float:left;">
          <input type="hidden" name="action" value="modificarpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $lineapedido['id'] ?>" />
          <input type="submit" value="Modificar" />
        </form>
        <form action="../controlador/admin.php" method="post">
          <input type="hidden" name="action" value="verpedido" />
          <input type="hidden" name="idpedido" value="<?php echo $lineapedido['id'] ?>" />
          <input type="submit" value="Ver" />
        </form>
      </td>
    </tr>

    <?php
  }
  ?>
  <tr><td>
        <form action="../controlador/pedidos.php" method="post">
          <input type="hidden" name="action" value="pedidocomprado" />
          <input type="hidden" name="idpedido" value="<?php echo $pedido['id'] ?>" />
          <input type="submit" value="Pedido comprado" />
        </form>
        </td></tr>
</tbody>
  </div>

                </div>
                </table>
                </div>
                </div>
