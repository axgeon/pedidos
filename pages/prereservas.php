
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de Reservas</div>
                
                <div class="panel-body">
<div class="container">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">id</th>
     <!-- <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">idreserva</th>-->
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">idcliente</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 255px;" aria-label="Engine version: activate to sort column ascending">nombrecliente</th>
      <!--<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">idvehiculo</th>-->
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Nombre Modelo</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">desde</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">hasta</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">oficinarecogida</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">oficinadevolucion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">precioextras</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">precioalquiler</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">preciototal</th>
  </thead>
  <tbody>
  <?php
  $i= 0;
  foreach ($reservas as $reserva) {?>

    <tr>
      <td><?php echo $reserva['id']; ?></td>
      <!--<td><?php echo $reserva['id_posiblereserva']; ?></td>-->
      <td><?php echo $reserva['id_cliente']; ?></td>
      <td><?php echo $datosreserva[$i]['nombrecliente']; ?></td>
      <!--<td><?php echo $reserva['id_vehiculo']; ?></td>-->
      <td><?php echo $datosreserva[$i]['nombremodelo']; ?></td>
      <td><?php echo $datosreserva[$i]['desde']; ?></td>
      <td><?php echo $datosreserva[$i]['hasta']; ?></td>
      <td><?php echo $datosreserva[$i]['oficinarecogida']; ?></td>
      <td><?php echo $datosreserva[$i]['oficinadevolucion']; ?></td>
      <td><?php echo $reserva['precio']; ?>€</td>
      <td><?php echo $reserva['precioextras']; ?>€</td>
      <td><?php echo $reserva['precioextras']+$reserva['precio']; ?>€</td>


    </tr>
    <?php
    $i++;
  }
  ?>
</tbody>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

