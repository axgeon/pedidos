  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Panel de control</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Inicio</div>
                
                <div class="panel-body">
                 <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $pedidos ?></div>
                                    <div>Pedidos de cliente!</div>
                                </div>
                            </div>
                        </div>
                        <a href="../controlador/pedidos.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver más</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $compraspendientes ?></div>
                                    <div>Compras pendientes!</div>
                                </div>
                            </div>
                        </div>
                        <a href="../controlador/compras.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver más</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-envira fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $plantasdisponibles ?></div>
                                    <div>Plantas activadas!</div>
                                </div>
                            </div>
                        </div>
                        <a href="../controlador/plantas.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver más</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $clientes ?></div>
                                    <div>Clientes!</div>
                                </div>
                            </div>
                        </div>
                        <a href="../controlador/clientes.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver más</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
</div>   
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Tabs
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Resumen</a>
                                </li>
                                <li><a href="#profile" data-toggle="tab">Necesidades</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home">
                                    <h4>Resumen</h4>

                                    <p>El objetivo es desarrollar un aplicativo para PlantaSalgar (en adelante la empresa) cuya funcionalidad principal será la de creación, gestión y procesamiento de pedidos.</p>
                                    <p>Por un lado existen los pedidos que los clientes realizan a la empresa, y por otro los que la empresa realizara a sus proveedores.</p>

                                    <p>En ambos casos se habla de generar una hoja de “prepedido” susceptible a cambios por el usuario  hasta el momento en que el pedido es formalizado.</p>

                                    <p>Dicha aplicación, contara con un módulo de Usuarios (login con usuario y contraseña) a desarrollar desde un principio puesto que está previsto implantar en un futuro próximo.</p>

                                    <p>En principio los usuarios no entraran a la plataforma para realizar los pedidos, en su lugar utilizaran una hoja de Excel que la empresa facilita, donde el cliente puede especificar qué productos desea en cuanta cantidad y luego remite dicha hoja a la empresa con el fin de proceder a realizar el pedido.</p>

                                    <p>Nota: de dicho Excel existen 2 versiones que varían los precios en función de si el usuario es tipo A o tipo B.(Diferencia marcada por volumen de compra)</p>

                                    <p>Se implementara un módulo para importar ficheros Excel a la Base de datos, de forma que con un solo click este pedido quede registrado en la BD.</p>
                                    <p>---</p>
                                    <h3>2a Reunion</h3>
                                    <p>Fecha de compra, fecha de pedido, proveedores</p>
                                    <p>Elegir proveedor</p>
                                    <p>Separar plantas por cliente en compras</p>
                                    <p>Y en pedido poder generar</p>
                                    <p>Como saber si el pedido esta realizado</p>
                                    <p>"No me ha llegado el pedido"</p>
                                    <p>Compras realizadas y compras pendientes</p>
                                    <p>Mover de pendiente a realizado de forma manual</p>
                                    <p>Cliente numero tal</p>
                                    <p>pedido numero tal de cliente</p>
                                    <p>Pedido numero tal de proveedor</p>
                                    <p>Duplicar pedido -> nuevo borrador</p>
                                    <p>envio, pedido con fallos, 3 pedidos pendientes</p>
                                    <p>Sistema de tareas</p>
                                    <p>Cambiar fotos de plantas</p>

                         </div>
                                <div class="tab-pane fade" id="profile">
                                    <h4>Necesidades</h4>
                                <ul>
                                    <li>Login user y password <i class="fa fa-check"></i></li>
                                    

                                    <li>Prepedido cliente<i class="fa fa-check"></i></li>
                                    <li>Pedido cliente + calculos Salgar <i class="fa fa-check"></i></li>
                                    <li>Prepedido Proveedor</li>
                                    <li>Pedido a Proveedor</li>
                                    <li>2 tipos de excel <i class="fa fa-check"></i></li>
                                        <ul>
                                            <li>excel 1 - 3 precios por volumen <i class="fa fa-check"></i></li>
                                            <li>excel 2 - 1 precio por unidad <i class="fa fa-check"></i></li>
                                        </ul>
                                    <li>Correo semiautomatico<i class="fa fa-check"></i></li>
                                    <li>Pedido cliente y pedido proveedor > Eurowin (Mejora)</li>
                                    <li>Sistema de alertas <i class="fa fa-check"></i></li>
                                    <li>Ocupacion aproximada de carros</li>
                                    <li>Pedidos </li>
                                        <ul>
                                            <li>Orden inverso en listado<i class="fa fa-check"></i></li>
                                            <li>Crear tipo1 <i class="fa fa-check"></i></li>
                                            <li>Crear tipo2 <i class="fa fa-check"></i></li>
                                            <li>Ver Detalles<i class="fa fa-check"></i></li>
                                            <li>Ver Listado<i class="fa fa-check"></i></li>
                                            <li>Modificar </li>
                                            <li>Duplicar <i class="fa fa-check"></i></li>
                                            <li>Importacion excel pedido <i class="fa fa-check"></i></li>
                                        </ul>
                                    <li>Compras </li>
                                        <ul>
                                            <li>Crear </li>
                                            <li>Ver <i class="fa fa-check"></i></li>
                                            <li>Modificar </li>
                                        </ul>                                        
                                    <li>Plantas</li>
                                        <ul>
                                            <li>Crear</li>
                                            <li>Ver listado<i class="fa fa-check"></i></li>
                                            <li>Ver listado Disponible <i class="fa fa-check"></i></li>
                                            <li>Modificar</li>                                    
                                        </ul>
                                    <li>Clientes<i class="fa fa-check"></i> </li>
                                        <ul>
                                            <li>Crear <i class="fa fa-check"></i></li>
                                            <li>Ver <i class="fa fa-check"></i></li>
                                            <li>Modificar </li>
                                        </ul>
                                    <li>Proveedores <i class="fa fa-check"></i></li>
                                        <ul>
                                            <li>Crear <i class="fa fa-check"></i></li>
                                            <li>Ver <i class="fa fa-check"></i></li>
                                            <li>Modificar <i class="fa fa-check"></i></li>
                                        </ul>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                                    <!-- /.panel -->
                  </div>
                  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        <!-- /#page-wrapper -->




