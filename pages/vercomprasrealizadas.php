
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Compras</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Compras realizadas</div>
                
                <div class="panel-body">
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline pedidos" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending">id</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">idproveedor</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">idcliente</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">total</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">fecha</th>
     <!--<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">confirmado</th>-->
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($pedidos as $pedido) {?>
    <tr>
      <td><?php echo $pedido['id'] ?></td>
      <td><?php echo $pedido['idproveedor'] ?></td>
      <td><?php echo $pedido['idcliente'] ?></td>
      <td><?php echo $pedido['total'] ?></td>
      <td><?php echo $pedido['fecha'] ?></td>
      <!--<td><?php echo $pedido['confirmado'] ?></td>-->

      <td>
<a href="mailto:manish@simplygraphix.com?subject=Feedback for webdevelopersnotes.com&body=The Tips and Tricks section is great">Send me an email</a>
      </td>
      
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
</table>
