
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de Posibles Reservas</div>
                
                <div class="panel-body">
<div class="container">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">id</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">Oficina Recogida</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 255px;" aria-label="Engine version: activate to sort column ascending">Fecha Recogida</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Hora Recogida</th>
      
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Oficina devolucion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Fecha devolucion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Hora devolucion</th>
      
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($posiblesreservas as $posiblereserva) {?>
    <tr>
      <td><?php echo $posiblereserva['id'] ?></td>
      <td><?php echo $db->getNombreOficina($posiblereserva['oficinarecogida']) ?></td>
      <td><?php
          $fechaconyear = $posiblereserva['fecharecogida']; 
          $parte = explode("-", $fechaconyear);
          //var_dump($parte);
          $fechaconyear = $parte[2]."-".$parte[1]."-".$parte[0];
          echo $fechaconyear; 
          ?></td>
      <td><?php echo $posiblereserva['horarecogida'] ?>:<?php echo $posiblereserva['minutosrecogida'] ?></td>
      <td><?php echo $db->getNombreOficina($posiblereserva['oficinadevolucion']) ?></td>
      <td><?php 
          $fechaconyear = $posiblereserva['fechadevolucion']; 
          $parte = explode("-", $fechaconyear);
          //var_dump($parte);
          $fechaconyear = $parte[2]."-".$parte[1]."-".$parte[0];

      echo $fechaconyear; ?></td>
      <td><?php echo $posiblereserva['horadevolucion'] ?>:<?php echo $posiblereserva['minutosdevolucion'] ?></td>
      <td>
        <!--<form action="../controlador/admin.php" type="post">
          <input type="hidden" value="<?php echo $vehiculo['id'] ?>" />
          <input type="submit" value="Modificar" />-->
        </form>
      </td>
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

