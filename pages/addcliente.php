
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clientes</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Nuevo cliente</div>
                
                <div class="panel-body">
<div class="container">

                <form action="../controlador/addcliente.php" method="post">
  <input type="hidden" name="action" value="insertarcliente" />
    <div class="form-group row">
      <label for="inputnombre" class="col-sm-2 col-form-label">Nombre</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputnombre" name="nombre" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputnombre2" class="col-sm-2 col-form-label">Nombre2</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputnombre2" name="nombre2" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputdireccion" class="col-sm-2 col-form-label">Direccion</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputdireccion" name="direccion" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputcodpost" class="col-sm-2 col-form-label">codpost</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputcodpost" name="codpost" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputpoblacion" class="col-sm-2 col-form-label">poblacion</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputpoblacion" name="poblacion" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputprovincia" class="col-sm-2 col-form-label">provincia</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputprovincia" name="provincia" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputcif" class="col-sm-2 col-form-label">cif</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputcif" name="cif" style="width:100%;">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputemail" class="col-sm-2 col-form-label">email</label>
      <div class="col-sm-10">
      <input type="text" class="form-control" id="inputemail" name="email" style="width:100%;">
      </div>
    </div>


    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </form>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

