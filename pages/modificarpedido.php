
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pedidos</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Crear pedido</div>
                
                <div class="panel-body">

                <?php
                if ($tarifa == 1){
                  echo '<form action="../controlador/crearpedido.php" method="post">';
                }else{
                  echo '<form action="../controlador/crearpedido2.php" method="post">';
                }
                ?>
  <input type="hidden" name="idpedido" value="<?php echo $pedido['id'] ?>" />              
  <input type="hidden" name="action" value="modificarpedidoform" />
        <div class="form-group">
          <label for="sel1">Cliente:</label>
          <select class="form-control" id="sel1" name="codigo">
          <?php foreach($clientes as $cliente){
            echo "<option value='".$cliente['codigo']."'>".$cliente['codigo']." - ".$cliente['nombre']."</option>";
          }?>
          </select>
        </div>  



  <div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline nowrap" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Carros</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Browser: activate to sort column ascending">Bases</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Unidades</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Codigo</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Descripción</th>
                     <?php
                if ($tarifa == 1){
                  echo '<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Precio en carro</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Precio en base</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Precio en unidad</th>';
                }else{
                  echo '<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Precio</th>';
                }
                ?>

     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Planta por carro</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Planta por base</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">% Ocupación</th>
     <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"  aria-label="Platform(s): activate to sort column ascending">Imagen</th>
  </thead>
  <tbody>
  <tr><td colspan="12"> Plantas en pedido</td></tr>
  <?php
  $codigosusados = array();
  foreach ($lineaspedido as $lineapedido) {
    $codigosusados[] = $lineapedido['codigo']?>
  <tr>
      <td><input type="number" value="<?php echo $lineapedido['pedidocarros']?>" name="carros_<?php echo $lineapedido['codigo'] ?>"></input></td>
      <td><input type="number" value="<?php echo $lineapedido['pedidobases']?>" name="bases_<?php echo $lineapedido['codigo'] ?>"></input></td>
      <td><input type="number" value="<?php echo $lineapedido['pedidounidades']?>" name="unidades_<?php echo $lineapedido['codigo'] ?>"></input></td>
      <td><?php echo $lineapedido['codigo'] ?></td>
      <td><?php echo $lineapedido['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$lineapedido["precio_carro"].'€ </td>
                        <td>'.$lineapedido["precio_base"].'€</td>
                        <td>'.$lineapedido["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$lineapedido["precio_especial"].'€</td>';
                }
                ?>

      <td><?php echo $lineapedido['plantas_carro'] ?></td>
      <td><?php echo $lineapedido['plantas_base'] ?></td>
      <td><?php echo $lineapedido['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $lineapedido['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
  <?php }?>
  <tr><td colspan="12"> Plantas tipo 1</td></tr>
  <?php
  foreach ($plantastipo1 as $plantatipo1) {
    if(!in_array($plantatipo1['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo1['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo1['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo1['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo1['codigo'] ?></td>
      <td><?php echo $plantatipo1['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo1["precio_carro"].'€ </td>
                        <td>'.$plantatipo1["precio_base"].'€</td>
                        <td>'.$plantatipo1["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo1["precio_especial"].'€</td>';
                }
                ?>

      <td><?php echo $plantatipo1['plantas_carro'] ?></td>
      <td><?php echo $plantatipo1['plantas_base'] ?></td>
      <td><?php echo $plantatipo1['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo1['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
    }else{

    }
  }
  ?>
   <tr><td colspan="12"> Plantas tipo 2</td></tr>
  <?php
  foreach ($plantastipo2 as $plantatipo2) {
    if(!in_array($plantatipo2['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo2['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo2['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo2['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo2['codigo'] ?></td>
      <td><?php echo $plantatipo2['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo2["precio_carro"].'€ </td>
                        <td>'.$plantatipo2["precio_base"].'€</td>
                        <td>'.$plantatipo2["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo2["precio_especial"].'€</td>';
                }
                ?>
      <td><?php echo $plantatipo2['plantas_carro'] ?></td>
      <td><?php echo $plantatipo2['plantas_base'] ?></td>
      <td><?php echo $plantatipo2['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo2['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
  }else{

  }
  }
  ?>
   <tr><td colspan="12"> Plantas tipo 3</td></tr>
  <?php
  foreach ($plantastipo3 as $plantatipo3) {
    if(!in_array($plantatipo1['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo3['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo3['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo3['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo3['codigo'] ?></td>
      <td><?php echo $plantatipo3['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo3["precio_carro"].'€ </td>
                        <td>'.$plantatipo3["precio_base"].'€</td>
                        <td>'.$plantatipo3["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo3["precio_especial"].'€</td>';
                }
                ?>
      <td><?php echo $plantatipo3['plantas_carro'] ?></td>
      <td><?php echo $plantatipo3['plantas_base'] ?></td>
      <td><?php echo $plantatipo3['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo3['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
    }else{}
  }

  ?>
   <tr><td colspan="12"> Plantas tipo 4</td></tr>
  <?php
  foreach ($plantastipo4 as $plantatipo4) {
    if(!in_array($plantatipo1['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo4['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo4['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo4['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo4['codigo'] ?></td>
      <td><?php echo $plantatipo4['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo4["precio_carro"].'€ </td>
                        <td>'.$plantatipo4["precio_base"].'€</td>
                        <td>'.$plantatipo4["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo4["precio_especial"].'€</td>';
                }
                ?>
      <td><?php echo $plantatipo4['plantas_carro'] ?></td>
      <td><?php echo $plantatipo4['plantas_base'] ?></td>
      <td><?php echo $plantatipo4['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo4['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
  }else{

  }
  }
  ?>
   <tr><td colspan="12"> Plantas tipo 5</td></tr>
  <?php
  foreach ($plantastipo5 as $plantatipo5) {
    if(!in_array($plantatipo1['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo5['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo5['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo5['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo5['codigo'] ?></td>
      <td><?php echo $plantatipo5['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo5["precio_carro"].'€ </td>
                        <td>'.$plantatipo5["precio_base"].'€</td>
                        <td>'.$plantatipo5["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo5["precio_especial"].'€</td>';
                }
                ?>
      <td><?php echo $plantatipo5['plantas_carro'] ?></td>
      <td><?php echo $plantatipo5['plantas_base'] ?></td>
      <td><?php echo $plantatipo5['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo5['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
  }else{

  }
}
  ?>
   <tr><td colspan="12"> Plantas tipo 6</td></tr>
  <?php
  foreach ($plantastipo6 as $plantatipo6) {
    if(!in_array($plantatipo1['codigo'], $codigosusados)){?>
    <tr>
      <td><input type="number" value="0" name="carros_<?php echo $plantatipo6['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="bases_<?php echo $plantatipo6['codigo'] ?>"></input></td>
      <td><input type="number" value="0" name="unidades_<?php echo $plantatipo6['codigo'] ?>"></input></td>
      <td><?php echo $plantatipo6['codigo'] ?></td>
      <td><?php echo $plantatipo6['descripcion'] ?></td>
                      <?php
                if ($tarifa == 1){
                  echo '<td>'.$plantatipo6["precio_carro"].'€ </td>
                        <td>'.$plantatipo6["precio_base"].'€</td>
                        <td>'.$plantatipo6["precio_unidad"].'€ </td>';
                }else{
                  echo '<td>'.$plantatipo6["precio_especial"].'€</td>';
                }
                ?>
      <td><?php echo $plantatipo6['plantas_carro'] ?></td>
      <td><?php echo $plantatipo6['plantas_base'] ?></td>
      <td><?php echo $plantatipo6['ocup'] ?></td>
      <td><a href="http://www.plantassalgar.com/disponible/img_plantas/<?php echo $plantatipo6['codigo'] ?>.jpg" target="_blank"><i class="fa fa-image"></i></a></td>

      
      
    </tr>
    <?php
  }else{

  }
}
  ?>
</tbody>
</table>
</div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Enviar</button>
      </div>
    </div>
  </form>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

