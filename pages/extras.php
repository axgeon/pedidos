
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de vehiculos</div>
                
                <div class="panel-body">
<div class="container">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">id</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">idreserva</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 255px;" aria-label="Engine version: activate to sort column ascending">Ctd bebe</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Ctd infantil</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Ctd elevador</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€ GPS</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€ seguro 1</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€ seguro 2</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">€ seguro 3</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Coste total</th>
  </thead>
  <tbody>
  <?php
  foreach ($extras as $extra) {?>
    <tr>
      <td><?php echo $extra['id'] ?></td>
      <td><?php echo $extra['idreserva'] ?></td>
      <td><?php echo $extra['cantidadbebe'] ?></td>
      <td><?php echo $extra['preciobebe'] ?></td>
      <td><?php echo $extra['cantidadsillainfantil'] ?></td>
      <td><?php echo $extra['preciosillainfantil'] ?></td>
      <td><?php echo $extra['cantidadelevador'] ?></td>
      <td><?php echo $extra['precioelevador'] ?></td>
      <td><?php echo $extra['preciogps'] ?></td>
      <td><?php echo $extra['precioseguro1'] ?></td>
      <td><?php echo $extra['precioseguro2'] ?></td>
      <td><?php echo $extra['precioseguro3'] ?></td>
      <td><?php echo $db->getPrecioTotalExtras($extra['id']); ?></td>
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

