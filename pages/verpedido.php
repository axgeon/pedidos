
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-9">

                    <h1 class="page-header">Pedidos</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Pedido #<?php echo $pedido['id'] ?></div>
                
                <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
                    <thead>
                      <tr role="row">
                          <!--<th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;"     aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">idpedido</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;"     aria-label="Browser: activate to sort column ascending">idlinea</th>-->
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform    (s): activate to sort column ascending">Codigo</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform    (s): activate to sort column ascending">Nombre</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">Carros</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">Bases</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">Unidades</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">€ Carros</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">€ Bases</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">€ Unidades</th>
                          <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="    Browser: activate to sort column ascending">Total</th>                        
                          <!--<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column     ascending">Acciones</th>-->
                        </tr>
                    </thead>
                   <tbody>
                   <?php
                   foreach ($lineaspedido as $lineapedido) {?>
                     <tr>
                       <!--<td><?php echo $lineapedido['idpedido'] ?></td>
                       <td><?php echo $lineapedido['idlinea'] ?></td>-->
                       <td><?php echo $lineapedido['codigo'] ?></td>
                       <td><?php echo $lineapedido['descripcion'] ?></td>
                       <td><?php echo $lineapedido['pedidocarros'] ?></td>
                       <td><?php echo $lineapedido['pedidobases'] ?></td>
                       <td><?php echo $lineapedido['pedidounidades'] ?></td>
                             <td><?php echo $lineapedido['totalcarros'] ?></td>
                       <td><?php echo $lineapedido['totalbases'] ?>€</td>
                       <td><?php echo $lineapedido['totalunidades'] ?>€</td>
                       <td><?php echo $lineapedido['totallinea'] ?>€</td>


<!--
                       <td>
                         <form action="../controlador/admin.php" method="post" style="float:left;">
                           <input type="hidden" name="action" value="modificarpedido" />
                           <input type="hidden" name="idpedido" value="<?php echo $lineapedido['id'] ?>" />
                           <input type="submit" value="Modificar" />
                         </form>
                         <form action="../controlador/admin.php" method="post">
                           <input type="hidden" name="action" value="verpedido" />
                           <input type="hidden" name="idpedido" value="<?php echo $lineapedido['id'] ?>" />
                           <input type="submit" value="Ver" />
                         </form>
                       </td>
                     </tr>
-->
                     <?php
                   }
                   ?>
                   <tr><td>
                         <form action="../controlador/pedidos.php" method="post">
                           <input type="hidden" name="action" value="confirmarpedido" />
                           <input type="hidden" name="idpedido" value="<?php echo $_POST['idpedido'] ?>" />
                           <input type="submit" value="Confirmar" />
                         </form>
                         </td>
                         <td>Total: <?php echo $pedido['total'] ?>€</td>
                   </tr>

                 </tbody>
                 </table>
                 </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="col-lg-3">
                <h1 class="page-header">Cliente</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">cliente #<?php echo $cliente['codigo'] ?></div>

                <div class="panel-body">
                <p><?php echo $cliente['nombre'] ?></p>
                <p><?php echo $cliente['nombre2'] ?></p>
                <p><?php echo $cliente['direccion'] ?></p>
                <p><?php echo $cliente['poblacion'] ?></p>
                <p><?php echo $cliente['provincia'] ?></p>
                <p><?php echo $cliente['codpost'] ?></p>
            </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

