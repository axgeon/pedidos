
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clientes</h1>
                
                <div class="panel panel-primary">
                <div class="panel-heading">Listado de Clientes</div>
                
                <div class="panel-body">
<table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 100%;" width="100%">
  <thead>
      <tr role="row">
      <th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 293px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">codigo</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 335px;" aria-label="Browser: activate to sort column ascending">nombre</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 306px;" aria-label="Platform(s): activate to sort column ascending">nombre2</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 255px;" aria-label="Engine version: activate to sort column ascending">direccion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">codpost</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">poblacion</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">provincia</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">cif</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">email</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">tipo</th>
      <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">registro</th>
      <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 189px;" aria-label="CSS grade: activate to sort column ascending">Acciones</th></tr>
  </thead>
  <tbody>
  <?php
  foreach ($clientes as $cliente) {?>
    <tr>
      <td><?php echo $cliente['codigo'] ?></td>
      <td><?php echo $cliente['nombre'] ?></td>
      <td><?php echo $cliente['nombre2'] ?></td>
      <td><?php echo $cliente['direccion'] ?></td>
      <td><?php echo $cliente['codpost'] ?></td>
      <td><?php echo $cliente['poblacion'] ?></td>
      <td><?php echo $cliente['provincia'] ?></td>
      <td><?php echo $cliente['cif'] ?></td>
      <td><?php echo $cliente['email'] ?></td>
      <td><?php echo $cliente['tipo'] ?></td>
      <td><?php echo $cliente['registro'] ?></td>
      <td>
        <form action="../controlador/clientes.php" method="post">
        <input type="hidden" name="codigo" value="<?php echo $cliente['codigo'] ?>" />
        <input type="hidden" name="action" value="modificar" />
          <input type="submit" value="Modificar" />
        </form>
       <form action="../controlador/clientes.php" method="post">
        <input type="hidden" name="codigo" value="<?php echo $cliente['codigo'] ?>" />
        <input type="hidden" name="action" value="historial" />
          <input type="submit" value="Historial" />
        </form>
      </td>
    </tr>
    <?php
  }
  ?>
</tbody>
  </div>
                </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

