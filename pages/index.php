<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PlantasSalgar Gestor de pedidos</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
#side-menu input[type="submit"]{
    background:white;
    color:#337ab7;
    margin-left:40px;
}
#side-menu .iconocolgandero{
color:#337ab7;
margin-left: 15px;
}
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.html">PlantasSalgar</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <img src="http://www.plantassalgar.com/images/Logo_.png" />
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    <li>
                        <a href="../controlador/admin.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                    </li>
                        
                    <li>
                        <a href="#"><i class="fa fa-file fa-fw"></i> Pedidos<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="../controlador/crearpedido.php">Crear pedido T.1</a>
                            </li>
                             <li>
                                <a href="../controlador/crearpedido2.php">Crear pedido T.2</a>
                            </li>
                            <li>
                                <a href="../controlador/pedidos.php">Listado Pedidos</a>
                            </li>
                            <li>
                                <a href="../controlador/importarpedidos.php">Importar Excel</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Compras<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="../controlador/compras.php">Compras pendientes</a>
                            </li>
                            <li>
                                <a href="../controlador/realizarcompras.php">Total pendiente de comprar</a>
                            </li>
                            <li>
                                <a href="../controlador/realizarcompras2.php">Realizar compra manual</a>
                            </li>
                             <li>
                                <a href="../controlador/comprasrealizadas.php">Compras realizadas</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>                    
                    <li>
                        <a href="#"><i class="fa fa-envira fa-fw"></i> Plantas<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../controlador/plantas.php">Listado maestro</a>
                            </li>
                            <li>
                                <a href="../controlador/plantasdisponibles.php">Listado disponible</a>
                            </li>
                             <li>
                                <a href="../controlador/plantasbyproveedor.php">Listado Proveedores</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i> Clientes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../controlador/clientes.php">Listado</a>
                            </li>
                            <li>
                                <a href="../controlador/addcliente.php">Crear cliente</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i> Proveedores<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../controlador/proveedores.php">Listado</a>
                            </li>
                            <li>
                                <a href="../controlador/addproveedor.php">Crear proveedor</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                            

                        <a href="#"><i class="fa fa-user fa-fw"></i> Cerrar Sesion<span class="fa arrow"></span></a>

                        <ul class="nav nav-second-level">
                            <li>
                                <form method="POST" action="../controlador/admin.php">
                                <input type="hidden" name="action" value="cerrar" />
                                <input type="submit" value="Cerrar Sesion" style="border:none;" />
                            </form>
                            </li>
                
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            
            <!-- /.navbar-static-side -->
        </nav>
